include c:\masm32\include\masm32rt.inc

;****************************************************
; Description : Example on how to replace occurrences 
;             : in a date string
; ---------------------------------------------------
; Example     :
;        Input: "a simple example of string 
;               replacement; the example is 
;               not optimized";
;
;       Search: "example";
;
;      Replace: "source";
;
;       Output: "a simple source that show string 
;                replacement; the source is not 
;                optimized"
; ---------------------------------------------------
; Author      : Marco C. (DispatchCode)
;****************************************************




; Data section
; #########################################################################################
.data

szString          db        "a simple example that show string replacement; the example is not optimized",0
SZ_LEN            equ       $-szString
szSearch          db        "example",0
szReplace         db        "source",0
crlf              db        13,10,0


; BSS section
; #########################################################################################
.data?
szOutput          db  SZ_LEN  dup(?)


; Code section
; #########################################################################################
.code
start:

print             "Original String: ",0
print    offset   szString

print    offset   crlf

print             "Search: ",0
print    offset   szSearch

print    offset   crlf

print             "Replace with: ",0
print    offset   szReplace

print    offset   crlf

call              main


inkey             "Press a key to exit..."

invoke            ExitProcess,0


main          proc

mov           ebx, offset szSearch
mov           esi, offset szString
mov           edi, offset szOutput

; original string loop
_while:
  cmp     byte ptr [esi], 0
  je      _exit_copy

  xor     ecx, ecx

  
  _substring:
    mov     al, byte ptr [esi]
    cmp     byte ptr [ebx+ecx], al
    jne     _exit_substr
    inc     ecx
    inc     esi
    
    jmp     _substring

  _exit_substr:

  ; The last char of ebx that we have seen,
  ; is 0?
  ; If YES, the substring is found
  cmp  byte ptr [ebx+ecx],0
  je      _substr_found

  ; Substring not found.
  sub     esi, ecx

  mov     al, byte ptr [esi]
  mov  byte ptr[edi], al

  inc     edi
  inc     esi

  jmp     _while

  _substr_found:
  
  ; Copy the substring into edi
  push    esi
  xor     esi, esi
  mov     esi, offset szReplace
  _copy_new:
    cmp   byte ptr [esi],0
    je      _exit_copy_found
    mov     al, byte ptr [esi]
    mov     byte ptr [edi], al

    inc     esi
    inc     edi
    jmp     _copy_new

  _exit_copy_found:
  pop     esi

  jmp     _while

_exit_copy:                       ; exit main loop
mov    byte ptr [edi], 0

print   offset crlf
print   offset szOutput

print   offset crlf
print   offset crlf


ret

main          endp


END             start