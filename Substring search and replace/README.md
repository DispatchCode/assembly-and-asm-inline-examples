# Search And Replace #

This is a little example that i wrote just for fun. It can search & replace occurrences into a text string without using any of the string instruction of the x86 platofm.


**Language:** MASM32

-----------

```
#!Assembly

szString          db        "a simple example that show string replacement; the example is not optimized",0
SZ_LEN            equ       $-szString
szSearch          db        "example",0
szReplace         db        "source",0

```

This will output:

```

a simple source that show string replacement; the source is not optimized

```