#Assembly and Assembly-inline Examples#

I wrote this sources only to demonstrate *how to* do *something* in Assembly language (or in Assembly inline with c).  This repo contains subdirectory that I add and/or edit when needed.

All the code that I wrote support only the Windows OS, or when WinAPI is not present, the Intel x86 architecture.